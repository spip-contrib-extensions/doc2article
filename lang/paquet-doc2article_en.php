<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-doc2article?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'doc2article_description' => 'Import all the files from tmp/upload and automatically create an article per file.',
	'doc2article_slogan' => 'Import all the files from tmp/upload and automatically create an article per file.'
);

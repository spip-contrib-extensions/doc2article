<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/doc2article?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_importer' => 'Importar',
	'bouton_supprimer' => 'Cancelar',

	// E
	'erreur_creation_article' => 'Error durante la creación del artículo',
	'erreur_creation_document' => 'Error al añadir el documento',
	'erreur_fichiers' => 'Debe seleccionar por lo menos un archivo',
	'erreur_repertoire' => 'Ningún archivo disponible. Primero tiene que subir unos en la carpeta: "@repertoire@".',
	'erreur_suppression_impossible' => '<p>No se puede borrar el archivo "@fichier@" de la carpeta temporal.</p><p>Verifique los derechos de la carpeta y de los archivos.',
	'explication_auteur' => 'Seleccione el autor de los artículos a crear.',
	'explication_fichiers' => 'Seleccione los archivos a procesar.',
	'explication_file_attente' => 'Esta página lista los documentos presentes en la cola. Esos serán importados automáticamente por CRON. Si lo desea, puede cancelar o forzar la importación de cada documento.',
	'explication_rubrique' => 'Seleccione la sección de los artículos a crear.',

	// I
	'icone_doc2article' => 'Importar los medios',

	// L
	'label_auteur' => 'Autor',
	'label_fichiers' => 'Archivos',
	'label_repertoire' => 'Carpeta',
	'label_rubrique' => 'Sección',
	'lien_file_attente' => 'Ver la cola.',
	'liste_aucun' => 'Ningún documento en la cola.',
	'liste_tous' => 'Documentos presentes en la cola.',

	// M
	'message_ajout_ok' => 'Los documentos fueron añadidos a la cola.',

	// N
	'nb_file_attente' => '@nb@ documento(s) en la cola.',

	// T
	'titre_bloc_import' => 'Importación de medios',
	'titre_doc2article' => 'doc2article',
	'titre_page_doc2article' => 'Módulo de importación de medios',
	'titre_page_file' => 'Cola del módulo de importación de medios'
);

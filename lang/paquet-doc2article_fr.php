<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/doc2article/trunk/lang
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'doc2article_description' => 'Importer l’ensemble des fichiers du répertoire tmp/upload et créer un article par fichier automatiquement.',
	'doc2article_slogan' => 'Importer l’ensemble des fichiers du répertoire tmp/upload et créer un article par fichier automatiquement'
);

<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/doc2article/trunk/lang
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_importer' => 'Importer',
	'bouton_supprimer' => 'Annuler',

	// E
	'erreur_creation_article' => 'Erreur lors de la création de l’article',
	'erreur_creation_document' => 'Erreur lors de l’ajout du document',
	'erreur_fichiers' => 'Vous devez sélectionner au moins un fichier',
	'erreur_repertoire' => 'Aucun fichier n’est disponible. Veuillez en uploader dans le répertoire : "@repertoire@".',
	'erreur_suppression_impossible' => '<p>Il est impossible de supprimer le fichier "@fichier@" du dossier temporaire.</p><p>Veuillez vérifier les droits du répertoire et des fichiers.',
	'explication_auteur' => 'Sélectionnez l’auteur des articles à créer.',
	'explication_fichiers' => 'Sélectionnez les fichiers à traiter.',
	'explication_file_attente' => 'Cette page liste les documents présents dans la file d’attente. Ceux-ci seront importés automatiquement par CRON. Si vous le souhaitez, vous pouvez annuler ou forcer l’import de chaque document.',
	'explication_rubrique' => 'Sélectionnez la rubrique des articles à créer.',

	// I
	'icone_doc2article' => 'Importer les Medias',

	// L
	'label_auteur' => 'Auteur',
	'label_fichiers' => 'Fichiers',
	'label_repertoire' => 'Répertoire',
	'label_rubrique' => 'Rubrique',
	'lien_file_attente' => 'Voir la file d’attente.',
	'liste_aucun' => 'Aucun document dans la file d’attente.',
	'liste_tous' => 'Documents présents dans la file d’attente.',

	// M
	'message_ajout_ok' => 'Les documents ont été ajoutés à la file d’attente.',

	// N
	'nb_file_attente' => '@nb@ document(s) dans la file d’attente.',

	// T
	'titre_bloc_import' => 'Importation de medias',
	'titre_doc2article' => 'doc2article',
	'titre_page_doc2article' => 'Module d’import des medias',
	'titre_page_file' => 'File d’attente du module d’import des medias'
);

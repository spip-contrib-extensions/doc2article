<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-doc2article?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'doc2article_description' => 'Importeer alle bestanden uit de map tmp/upload en maak van ieder bestand automatisch een artikel.',
	'doc2article_slogan' => 'Importeer alle bestanden uit de map tmp/upload en maak van ieder bestand automatisch een artikel'
);

<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/doc2article?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_importer' => 'Importeren',
	'bouton_supprimer' => 'Annuleren',

	// E
	'erreur_creation_article' => 'Fout bij het aanmaken van het artikel',
	'erreur_creation_document' => 'Fout bij het toevoegen van het document',
	'erreur_fichiers' => 'Je moet ten minste één bestand kiezen',
	'erreur_repertoire' => 'Er is geen enkel bestand beschikbaar. Laadt ze op in de map: "@repertoire@".',
	'erreur_suppression_impossible' => '<p>Het bestand "@fichier@" kan niet uit de tijdelijke map worden verwijderd.</p><p>Controleer de rechten van de map en de bestanden.</p>',
	'explication_auteur' => 'Kies de auteur voor de te maken artikelen.',
	'explication_fichiers' => 'Kies de te bewerken bestanden.',
	'explication_file_attente' => 'Deze bladzijde toont de doculenten die in de wachtrij staan. Deze zullen automatisch worden geïmporteerd door CRON. Wanneer je dat wil kun je ieder document verwijderen of de import forceren.',
	'explication_rubrique' => 'Kies de rubriek voor de te maken artikelen.',

	// I
	'icone_doc2article' => 'Media importeren',

	// L
	'label_auteur' => 'Auteur',
	'label_fichiers' => 'Bestanden',
	'label_repertoire' => 'Map',
	'label_rubrique' => 'Rubriek',
	'lien_file_attente' => 'De wachtrij bekijken.',
	'liste_aucun' => 'Er staan geen documenten in de wachtrij.',
	'liste_tous' => 'Documenten aanwezig in de wachtrij.',

	// M
	'message_ajout_ok' => 'De documents zijn aan de wachtrij toegevoegd.',

	// N
	'nb_file_attente' => '@nb@ document(en) in de wachtrij.',

	// T
	'titre_bloc_import' => 'Importeren van media',
	'titre_doc2article' => 'doc2article',
	'titre_page_doc2article' => 'Module voor import van media',
	'titre_page_file' => 'Wachtrij van de media import module'
);
